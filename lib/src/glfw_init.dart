//  Copyright 2019 root.ext@gmail.com
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import 'dart:ffi';

import 'glfw_header.dart';
import 'glfw_library.dart';

late final _library = loadLibrary();
GlfwInit glfwInit =
    loadFunction<GlfwInitNative>(_library, 'glfwInit').asFunction<GlfwInit>();
GlfwTerminate glfwTerminate =
    loadFunction<GlfwTerminateNative>(_library, 'glfwTerminate')
        .asFunction<GlfwTerminate>();
GlfwInitHint glfwInitHint =
    loadFunction<GlfwInitHintNative>(_library, 'glfwInitHint')
        .asFunction<GlfwInitHint>();
GlfwGetVersion glfwGetVersion =
    loadFunction<GlfwGetVersionNative>(_library, 'glfwGetVersion')
        .asFunction<GlfwGetVersion>();
GlfwGetVersionString glfwGetVersionString =
    loadFunction<GlfwGetVersionStringNative>(_library, 'glfwGetVersionString')
        .asFunction<GlfwGetVersionString>();
GlfwGetError glfwGetError =
    loadFunction<GlfwGetErrorNative>(_library, 'glfwGetError')
        .asFunction<GlfwGetError>();
GlfwSetErrorCallback glfwSetErrorCallback =
    loadFunction<GlfwSetErrorCallbackNative>(_library, 'glfwSetErrorCallback')
        .asFunction<GlfwSetErrorCallback>();
GlfwGetMonitors glfwGetMonitors =
    loadFunction<GlfwGetMonitorsNative>(_library, 'glfwGetMonitors')
        .asFunction<GlfwGetMonitors>();
GlfwGetPrimaryMonitor glfwGetPrimaryMonitor =
    loadFunction<GlfwGetPrimaryMonitorNative>(_library, 'glfwGetPrimaryMonitor')
        .asFunction<GlfwGetPrimaryMonitor>();
GlfwGetMonitorPos glfwGetMonitorPos =
    loadFunction<GlfwGetMonitorPosNative>(_library, 'glfwGetMonitorPos')
        .asFunction<GlfwGetMonitorPos>();
GlfwGetMonitorWorkarea glfwGetMonitorWorkarea =
    loadFunction<GlfwGetMonitorWorkareaNative>(
            _library, 'glfwGetMonitorWorkarea')
        .asFunction<GlfwGetMonitorWorkarea>();
GlfwGetMonitorPhysicalSize glfwGetMonitorPhysicalSize =
    loadFunction<GlfwGetMonitorPhysicalSizeNative>(
            _library, 'glfwGetMonitorPhysicalSize')
        .asFunction<GlfwGetMonitorPhysicalSize>();
GlfwGetMonitorContentScale glfwGetMonitorContentScale =
    loadFunction<GlfwGetMonitorContentScaleNative>(
            _library, 'glfwGetMonitorContentScale')
        .asFunction<GlfwGetMonitorContentScale>();
GlfwGetMonitorName glfwGetMonitorName =
    loadFunction<GlfwGetMonitorNameNative>(_library, 'glfwGetMonitorName')
        .asFunction<GlfwGetMonitorName>();
GlfwSetMonitorUserPointer glfwSetMonitorUserPointer =
    loadFunction<GlfwSetMonitorUserPointerNative>(
            _library, 'glfwSetMonitorUserPointer')
        .asFunction<GlfwSetMonitorUserPointer>();
GlfwGetMonitorUserPointer glfwGetMonitorUserPointer =
    loadFunction<GlfwGetMonitorUserPointerNative>(
            _library, 'glfwGetMonitorUserPointer')
        .asFunction<GlfwGetMonitorUserPointer>();
GlfwSetMonitorCallback glfwSetMonitorCallback =
    loadFunction<GlfwSetMonitorCallbackNative>(
            _library, 'glfwSetMonitorCallback')
        .asFunction<GlfwSetMonitorCallback>();
GlfwGetVideoModes glfwGetVideoModes =
    loadFunction<GlfwGetVideoModesNative>(_library, 'glfwGetVideoModes')
        .asFunction<GlfwGetVideoModes>();
GlfwGetVideoMode glfwGetVideoMode =
    loadFunction<GlfwGetVideoModeNative>(_library, 'glfwGetVideoMode')
        .asFunction<GlfwGetVideoMode>();
GlfwSetGamma glfwSetGamma =
    loadFunction<GlfwSetGammaNative>(_library, 'glfwSetGamma')
        .asFunction<GlfwSetGamma>();
GlfwGetGammaRamp glfwGetGammaRamp =
    loadFunction<GlfwGetGammaRampNative>(_library, 'glfwGetGammaRamp')
        .asFunction<GlfwGetGammaRamp>();
GlfwSetGammaRamp glfwSetGammaRamp =
    loadFunction<GlfwSetGammaRampNative>(_library, 'glfwSetGammaRamp')
        .asFunction<GlfwSetGammaRamp>();
GlfwDefaultWindowHints glfwDefaultWindowHints =
    loadFunction<GlfwDefaultWindowHintsNative>(
            _library, 'glfwDefaultWindowHints')
        .asFunction<GlfwDefaultWindowHints>();
GlfwWindowHint glfwWindowHint =
    loadFunction<GlfwWindowHintNative>(_library, 'glfwWindowHint')
        .asFunction<GlfwWindowHint>();
GlfwWindowHintString glfwWindowHintString =
    loadFunction<GlfwWindowHintStringNative>(_library, 'glfwWindowHintString')
        .asFunction<GlfwWindowHintString>();
GlfwCreateWindow glfwCreateWindow =
    loadFunction<GlfwCreateWindowNative>(_library, 'glfwCreateWindow')
        .asFunction<GlfwCreateWindow>();
GlfwDestroyWindow glfwDestroyWindow =
    loadFunction<GlfwDestroyWindowNative>(_library, 'glfwDestroyWindow')
        .asFunction<GlfwDestroyWindow>();
GlfwWindowShouldClose glfwWindowShouldClose =
    loadFunction<GlfwWindowShouldCloseNative>(_library, 'glfwWindowShouldClose')
        .asFunction<GlfwWindowShouldClose>();
GlfwSetWindowShouldClose glfwSetWindowShouldClose =
    loadFunction<GlfwSetWindowShouldCloseNative>(
            _library, 'glfwSetWindowShouldClose')
        .asFunction<GlfwSetWindowShouldClose>();
GlfwSetWindowTitle glfwSetWindowTitle =
    loadFunction<GlfwSetWindowTitleNative>(_library, 'glfwSetWindowTitle')
        .asFunction<GlfwSetWindowTitle>();
GlfwSetWindowIcon glfwSetWindowIcon =
    loadFunction<GlfwSetWindowIconNative>(_library, 'glfwSetWindowIcon')
        .asFunction<GlfwSetWindowIcon>();
GlfwGetWindowPos glfwGetWindowPos =
    loadFunction<GlfwGetWindowPosNative>(_library, 'glfwGetWindowPos')
        .asFunction<GlfwGetWindowPos>();
GlfwSetWindowPos glfwSetWindowPos =
    loadFunction<GlfwSetWindowPosNative>(_library, 'glfwSetWindowPos')
        .asFunction<GlfwSetWindowPos>();
GlfwGetWindowSize glfwGetWindowSize =
    loadFunction<GlfwGetWindowSizeNative>(_library, 'glfwGetWindowSize')
        .asFunction<GlfwGetWindowSize>();
GlfwSetWindowSizeLimits glfwSetWindowSizeLimits =
    loadFunction<GlfwSetWindowSizeLimitsNative>(
            _library, 'glfwSetWindowSizeLimits')
        .asFunction<GlfwSetWindowSizeLimits>();
GlfwSetWindowAspectRatio glfwSetWindowAspectRatio =
    loadFunction<GlfwSetWindowAspectRatioNative>(
            _library, 'glfwSetWindowAspectRatio')
        .asFunction<GlfwSetWindowAspectRatio>();
GlfwSetWindowSize glfwSetWindowSize =
    loadFunction<GlfwSetWindowSizeNative>(_library, 'glfwSetWindowSize')
        .asFunction<GlfwSetWindowSize>();
GlfwGetFramebufferSize glfwGetFramebufferSize =
    loadFunction<GlfwGetFramebufferSizeNative>(
            _library, 'glfwGetFramebufferSize')
        .asFunction<GlfwGetFramebufferSize>();
GlfwGetWindowFrameSize glfwGetWindowFrameSize =
    loadFunction<GlfwGetWindowFrameSizeNative>(
            _library, 'glfwGetWindowFrameSize')
        .asFunction<GlfwGetWindowFrameSize>();
GlfwGetWindowContentScale glfwGetWindowContentScale =
    loadFunction<GlfwGetWindowContentScaleNative>(
            _library, 'glfwGetWindowContentScale')
        .asFunction<GlfwGetWindowContentScale>();
GlfwGetWindowOpacity glfwGetWindowOpacity =
    loadFunction<GlfwGetWindowOpacityNative>(_library, 'glfwGetWindowOpacity')
        .asFunction<GlfwGetWindowOpacity>();
GlfwSetWindowOpacity glfwSetWindowOpacity =
    loadFunction<GlfwSetWindowOpacityNative>(_library, 'glfwSetWindowOpacity')
        .asFunction<GlfwSetWindowOpacity>();
GlfwIconifyWindow glfwIconifyWindow =
    loadFunction<GlfwIconifyWindowNative>(_library, 'glfwIconifyWindow')
        .asFunction<GlfwIconifyWindow>();
GlfwRestoreWindow glfwRestoreWindow =
    loadFunction<GlfwRestoreWindowNative>(_library, 'glfwRestoreWindow')
        .asFunction<GlfwRestoreWindow>();
GlfwMaximizeWindow glfwMaximizeWindow =
    loadFunction<GlfwMaximizeWindowNative>(_library, 'glfwMaximizeWindow')
        .asFunction<GlfwMaximizeWindow>();
GlfwShowWindow glfwShowWindow =
    loadFunction<GlfwShowWindowNative>(_library, 'glfwShowWindow')
        .asFunction<GlfwShowWindow>();
GlfwHideWindow glfwHideWindow =
    loadFunction<GlfwHideWindowNative>(_library, 'glfwHideWindow')
        .asFunction<GlfwHideWindow>();
GlfwFocusWindow glfwFocusWindow =
    loadFunction<GlfwFocusWindowNative>(_library, 'glfwFocusWindow')
        .asFunction<GlfwFocusWindow>();
GlfwRequestWindowAttention glfwRequestWindowAttention =
    loadFunction<GlfwRequestWindowAttentionNative>(
            _library, 'glfwRequestWindowAttention')
        .asFunction<GlfwRequestWindowAttention>();
GlfwGetWindowMonitor glfwGetWindowMonitor =
    loadFunction<GlfwGetWindowMonitorNative>(_library, 'glfwGetWindowMonitor')
        .asFunction<GlfwGetWindowMonitor>();
GlfwSetWindowMonitor glfwSetWindowMonitor =
    loadFunction<GlfwSetWindowMonitorNative>(_library, 'glfwSetWindowMonitor')
        .asFunction<GlfwSetWindowMonitor>();
GlfwGetWindowAttrib glfwGetWindowAttrib =
    loadFunction<GlfwGetWindowAttribNative>(_library, 'glfwGetWindowAttrib')
        .asFunction<GlfwGetWindowAttrib>();
GlfwSetWindowAttrib glfwSetWindowAttrib =
    loadFunction<GlfwSetWindowAttribNative>(_library, 'glfwSetWindowAttrib')
        .asFunction<GlfwSetWindowAttrib>();
GlfwSetWindowUserPointer glfwSetWindowUserPointer =
    loadFunction<GlfwSetWindowUserPointerNative>(
            _library, 'glfwSetWindowUserPointer')
        .asFunction<GlfwSetWindowUserPointer>();
GlfwGetWindowUserPointer glfwGetWindowUserPointer =
    loadFunction<GlfwGetWindowUserPointerNative>(
            _library, 'glfwGetWindowUserPointer')
        .asFunction<GlfwGetWindowUserPointer>();
GlfwSetWindowPosCallback glfwSetWindowPosCallback =
    loadFunction<GlfwSetWindowPosCallbackNative>(
            _library, 'glfwSetWindowPosCallback')
        .asFunction<GlfwSetWindowPosCallback>();
GlfwSetWindowSizeCallback glfwSetWindowSizeCallback =
    loadFunction<GlfwSetWindowSizeCallbackNative>(
            _library, 'glfwSetWindowSizeCallback')
        .asFunction<GlfwSetWindowSizeCallback>();
GlfwSetWindowCloseCallback glfwSetWindowCloseCallback =
    loadFunction<GlfwSetWindowCloseCallbackNative>(
            _library, 'glfwSetWindowCloseCallback')
        .asFunction<GlfwSetWindowCloseCallback>();
GlfwSetWindowRefreshCallback glfwSetWindowRefreshCallback =
    loadFunction<GlfwSetWindowRefreshCallbackNative>(
            _library, 'glfwSetWindowRefreshCallback')
        .asFunction<GlfwSetWindowRefreshCallback>();
GlfwSetWindowFocusCallback glfwSetWindowFocusCallback =
    loadFunction<GlfwSetWindowFocusCallbackNative>(
            _library, 'glfwSetWindowFocusCallback')
        .asFunction<GlfwSetWindowFocusCallback>();
GlfwSetWindowIconifyCallback glfwSetWindowIconifyCallback =
    loadFunction<GlfwSetWindowIconifyCallbackNative>(
            _library, 'glfwSetWindowIconifyCallback')
        .asFunction<GlfwSetWindowIconifyCallback>();
GlfwSetWindowMaximizeCallback glfwSetWindowMaximizeCallback =
    loadFunction<GlfwSetWindowMaximizeCallbackNative>(
            _library, 'glfwSetWindowMaximizeCallback')
        .asFunction<GlfwSetWindowMaximizeCallback>();
GlfwSetFramebufferSizeCallback glfwSetFramebufferSizeCallback =
    loadFunction<GlfwSetFramebufferSizeCallbackNative>(
            _library, 'glfwSetFramebufferSizeCallback')
        .asFunction<GlfwSetFramebufferSizeCallback>();
GlfwSetWindowContentScaleCallback glfwSetWindowContentScaleCallback =
    loadFunction<GlfwSetWindowContentScaleCallbackNative>(
            _library, 'glfwSetWindowContentScaleCallback')
        .asFunction<GlfwSetWindowContentScaleCallback>();
GlfwPollEvents glfwPollEvents =
    loadFunction<GlfwPollEventsNative>(_library, 'glfwPollEvents')
        .asFunction<GlfwPollEvents>();
GlfwWaitEvents glfwWaitEvents =
    loadFunction<GlfwWaitEventsNative>(_library, 'glfwWaitEvents')
        .asFunction<GlfwWaitEvents>();
GlfwWaitEventsTimeout glfwWaitEventsTimeout =
    loadFunction<GlfwWaitEventsTimeoutNative>(_library, 'glfwWaitEventsTimeout')
        .asFunction<GlfwWaitEventsTimeout>();
GlfwPostEmptyEvent glfwPostEmptyEvent =
    loadFunction<GlfwPostEmptyEventNative>(_library, 'glfwPostEmptyEvent')
        .asFunction<GlfwPostEmptyEvent>();
GlfwGetInputMode glfwGetInputMode =
    loadFunction<GlfwGetInputModeNative>(_library, 'glfwGetInputMode')
        .asFunction<GlfwGetInputMode>();
GlfwSetInputMode glfwSetInputMode =
    loadFunction<GlfwSetInputModeNative>(_library, 'glfwSetInputMode')
        .asFunction<GlfwSetInputMode>();
GlfwRawMouseMotionSupported glfwRawMouseMotionSupported =
    loadFunction<GlfwRawMouseMotionSupportedNative>(
            _library, 'glfwRawMouseMotionSupported')
        .asFunction<GlfwRawMouseMotionSupported>();
GlfwGetKeyName glfwGetKeyName =
    loadFunction<GlfwGetKeyNameNative>(_library, 'glfwGetKeyName')
        .asFunction<GlfwGetKeyName>();
GlfwGetKeyScancode glfwGetKeyScancode =
    loadFunction<GlfwGetKeyScancodeNative>(_library, 'glfwGetKeyScancode')
        .asFunction<GlfwGetKeyScancode>();
GlfwGetKey glfwGetKey = loadFunction<GlfwGetKeyNative>(_library, 'glfwGetKey')
    .asFunction<GlfwGetKey>();
GlfwGetMouseButton glfwGetMouseButton =
    loadFunction<GlfwGetMouseButtonNative>(_library, 'glfwGetMouseButton')
        .asFunction<GlfwGetMouseButton>();
GlfwGetCursorPos glfwGetCursorPos =
    loadFunction<GlfwGetCursorPosNative>(_library, 'glfwGetCursorPos')
        .asFunction<GlfwGetCursorPos>();
GlfwSetCursorPos glfwSetCursorPos =
    loadFunction<GlfwSetCursorPosNative>(_library, 'glfwSetCursorPos')
        .asFunction<GlfwSetCursorPos>();
GlfwCreateCursor glfwCreateCursor =
    loadFunction<GlfwCreateCursorNative>(_library, 'glfwCreateCursor')
        .asFunction<GlfwCreateCursor>();
GlfwCreateStandardCursor glfwCreateStandardCursor =
    loadFunction<GlfwCreateStandardCursorNative>(
            _library, 'glfwCreateStandardCursor')
        .asFunction<GlfwCreateStandardCursor>();
GlfwDestroyCursor glfwDestroyCursor =
    loadFunction<GlfwDestroyCursorNative>(_library, 'glfwDestroyCursor')
        .asFunction<GlfwDestroyCursor>();
GlfwSetCursor glfwSetCursor =
    loadFunction<GlfwSetCursorNative>(_library, 'glfwSetCursor')
        .asFunction<GlfwSetCursor>();
GlfwSetKeyCallback glfwSetKeyCallback =
    loadFunction<GlfwSetKeyCallbackNative>(_library, 'glfwSetKeyCallback')
        .asFunction<GlfwSetKeyCallback>();
GlfwSetCharCallback glfwSetCharCallback =
    loadFunction<GlfwSetCharCallbackNative>(_library, 'glfwSetCharCallback')
        .asFunction<GlfwSetCharCallback>();
GlfwSetCharModsCallback glfwSetCharModsCallback =
    loadFunction<GlfwSetCharModsCallbackNative>(
            _library, 'glfwSetCharModsCallback')
        .asFunction<GlfwSetCharModsCallback>();
GlfwSetMouseButtonCallback glfwSetMouseButtonCallback =
    loadFunction<GlfwSetMouseButtonCallbackNative>(
            _library, 'glfwSetMouseButtonCallback')
        .asFunction<GlfwSetMouseButtonCallback>();
GlfwSetCursorPosCallback glfwSetCursorPosCallback =
    loadFunction<GlfwSetCursorPosCallbackNative>(
            _library, 'glfwSetCursorPosCallback')
        .asFunction<GlfwSetCursorPosCallback>();
GlfwSetCursorEnterCallback glfwSetCursorEnterCallback =
    loadFunction<GlfwSetCursorEnterCallbackNative>(
            _library, 'glfwSetCursorEnterCallback')
        .asFunction<GlfwSetCursorEnterCallback>();
GlfwSetScrollCallback glfwSetScrollCallback =
    loadFunction<GlfwSetScrollCallbackNative>(_library, 'glfwSetScrollCallback')
        .asFunction<GlfwSetScrollCallback>();
GlfwSetDropCallback glfwSetDropCallback =
    loadFunction<GlfwSetDropCallbackNative>(_library, 'glfwSetDropCallback')
        .asFunction<GlfwSetDropCallback>();
GlfwJoystickPresent glfwJoystickPresent =
    loadFunction<GlfwJoystickPresentNative>(_library, 'glfwJoystickPresent')
        .asFunction<GlfwJoystickPresent>();
GlfwGetJoystickAxes glfwGetJoystickAxes =
    loadFunction<GlfwGetJoystickAxesNative>(_library, 'glfwGetJoystickAxes')
        .asFunction<GlfwGetJoystickAxes>();
GlfwGetJoystickButtons glfwGetJoystickButtons =
    loadFunction<GlfwGetJoystickButtonsNative>(
            _library, 'glfwGetJoystickButtons')
        .asFunction<GlfwGetJoystickButtons>();
GlfwGetJoystickHats glfwGetJoystickHats =
    loadFunction<GlfwGetJoystickHatsNative>(_library, 'glfwGetJoystickHats')
        .asFunction<GlfwGetJoystickHats>();
GlfwGetJoystickName glfwGetJoystickName =
    loadFunction<GlfwGetJoystickNameNative>(_library, 'glfwGetJoystickName')
        .asFunction<GlfwGetJoystickName>();
GlfwGetJoystickGUID glfwGetJoystickGUID =
    loadFunction<GlfwGetJoystickGUIDNative>(_library, 'glfwGetJoystickGUID')
        .asFunction<GlfwGetJoystickGUID>();
GlfwSetJoystickUserPointer glfwSetJoystickUserPointer =
    loadFunction<GlfwSetJoystickUserPointerNative>(
            _library, 'glfwSetJoystickUserPointer')
        .asFunction<GlfwSetJoystickUserPointer>();
GlfwGetJoystickUserPointer glfwGetJoystickUserPointer =
    loadFunction<GlfwGetJoystickUserPointerNative>(
            _library, 'glfwGetJoystickUserPointer')
        .asFunction<GlfwGetJoystickUserPointer>();
GlfwJoystickIsGamepad glfwJoystickIsGamepad =
    loadFunction<GlfwJoystickIsGamepadNative>(_library, 'glfwJoystickIsGamepad')
        .asFunction<GlfwJoystickIsGamepad>();
GlfwSetJoystickCallback glfwSetJoystickCallback =
    loadFunction<GlfwSetJoystickCallbackNative>(
            _library, 'glfwSetJoystickCallback')
        .asFunction<GlfwSetJoystickCallback>();
GlfwUpdateGamepadMappings glfwUpdateGamepadMappings =
    loadFunction<GlfwUpdateGamepadMappingsNative>(
            _library, 'glfwUpdateGamepadMappings')
        .asFunction<GlfwUpdateGamepadMappings>();
GlfwGetGamepadName glfwGetGamepadName =
    loadFunction<GlfwGetGamepadNameNative>(_library, 'glfwGetGamepadName')
        .asFunction<GlfwGetGamepadName>();
GlfwGetGamepadState glfwGetGamepadState =
    loadFunction<GlfwGetGamepadStateNative>(_library, 'glfwGetGamepadState')
        .asFunction<GlfwGetGamepadState>();
GlfwSetClipboardString glfwSetClipboardString =
    loadFunction<GlfwSetClipboardStringNative>(
            _library, 'glfwSetClipboardString')
        .asFunction<GlfwSetClipboardString>();
GlfwGetClipboardString glfwGetClipboardString =
    loadFunction<GlfwGetClipboardStringNative>(
            _library, 'glfwGetClipboardString')
        .asFunction<GlfwGetClipboardString>();
GlfwGetTime glfwGetTime =
    loadFunction<GlfwGetTimeNative>(_library, 'glfwGetTime')
        .asFunction<GlfwGetTime>();
GlfwSetTime glfwSetTime =
    loadFunction<GlfwSetTimeNative>(_library, 'glfwSetTime')
        .asFunction<GlfwSetTime>();
GlfwGetTimerValue glfwGetTimerValue =
    loadFunction<GlfwGetTimerValueNative>(_library, 'glfwGetTimerValue')
        .asFunction<GlfwGetTimerValue>();
GlfwGetTimerFrequency glfwGetTimerFrequency =
    loadFunction<GlfwGetTimerFrequencyNative>(_library, 'glfwGetTimerFrequency')
        .asFunction<GlfwGetTimerFrequency>();
GlfwMakeContextCurrent glfwMakeContextCurrent =
    loadFunction<GlfwMakeContextCurrentNative>(
            _library, 'glfwMakeContextCurrent')
        .asFunction<GlfwMakeContextCurrent>();
GlfwGetCurrentContext glfwGetCurrentContext =
    loadFunction<GlfwGetCurrentContextNative>(_library, 'glfwGetCurrentContext')
        .asFunction<GlfwGetCurrentContext>();
GlfwSwapBuffers glfwSwapBuffers =
    loadFunction<GlfwSwapBuffersNative>(_library, 'glfwSwapBuffers')
        .asFunction<GlfwSwapBuffers>();
GlfwSwapInterval glfwSwapInterval =
    loadFunction<GlfwSwapIntervalNative>(_library, 'glfwSwapInterval')
        .asFunction<GlfwSwapInterval>();
GlfwExtensionSupported glfwExtensionSupported =
    loadFunction<GlfwExtensionSupportedNative>(
            _library, 'glfwExtensionSupported')
        .asFunction<GlfwExtensionSupported>();
GlfwGetProcAddress glfwGetProcAddress =
    loadFunction<GlfwGetProcAddressNative>(_library, 'glfwGetProcAddress')
        .asFunction<GlfwGetProcAddress>();
GlfwVulkanSupported glfwVulkanSupported =
    loadFunction<GlfwVulkanSupportedNative>(_library, 'glfwVulkanSupported')
        .asFunction<GlfwVulkanSupported>();
GlfwGetRequiredInstanceExtensions glfwGetRequiredInstanceExtensions =
    loadFunction<GlfwGetRequiredInstanceExtensionsNative>(
            _library, 'glfwGetRequiredInstanceExtensions')
        .asFunction<GlfwGetRequiredInstanceExtensions>();
GlfwGetInstanceProcAddress glfwGetInstanceProcAddress =
    loadFunction<GlfwGetInstanceProcAddressNative>(
            _library, 'glfwGetInstanceProcAddress')
        .asFunction<GlfwGetInstanceProcAddress>();
GlfwGetPhysicalDevicePresentationSupport
    glfwGetPhysicalDevicePresentationSupport =
    loadFunction<GlfwGetPhysicalDevicePresentationSupportNative>(
            _library, 'glfwGetPhysicalDevicePresentationSupport')
        .asFunction<GlfwGetPhysicalDevicePresentationSupport>();
GlfwCreateWindowSurface glfwCreateWindowSurface =
    loadFunction<GlfwCreateWindowSurfaceNative>(
            _library, 'glfwCreateWindowSurface')
        .asFunction<GlfwCreateWindowSurface>();
